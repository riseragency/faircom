$(document).ready(function() {

    var owl = $(".owl-carousel");

    owl.owlCarousel({

        navigation : false,
        slideSpeed : 300,
        singleItem:true
    });

    var owlNav = $(".owl-nav");

    owlNav.owlCarousel({

        navigation : true,
        slideSpeed : 300,
        items:5,
        center:true,
        navigationText:["<i class='fa fa-angle-double-left'>","<i class='fa fa-angle-double-right'>"],
        dots:false
    });

    var owlNavSingle = $(".owl-nav-single");

    owlNavSingle.owlCarousel({

        navigation : true,
        slideSpeed : 300,
        singleItem:true,
        center:true,
        navigationText:["<i class='fa fa-angle-double-left'>","<i class='fa fa-angle-double-right'>"],
        dots:false
    });


});
