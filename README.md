Rinzler
========================

Rinzler facilitates your front-end cut ups, sets up a HTML boilerplate, and predefines CSS styles with LESS.

### HTML Preloaded Attributues

    - HTML Boilerplate
    - Mobile Viewport Support
    - [FontAwesome CDN] (http://fortawesome.github.io/Font-Awesome/)
    - jQuery CDN

### Predefined CSS / LESS Attributues

    - Robust reset styles
    - Responsive headers (H1-H6)
    - Responsive breakpoints desktop/tablet/mobile
    - [Expansive LESS Mixin Library] (https://css-tricks.com/snippets/css/useful-css3-less-mixins/)

### jQuery Features

    - Mobile animated nav
    - Accordion

### LESS Configuration

    * [Install LESS](https://github.com/less/less.js)

        npm install less


### Clone Locally

    HTTPS:  git clone https://github.com/erickolivares/rinzler.git
    SSH:    git clone https://github.com/erickolivares/rinzler.git
